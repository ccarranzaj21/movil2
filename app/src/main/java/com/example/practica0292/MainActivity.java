package com.example.practica0292;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText editTextWeight;
    private EditText editTextHeight;
    private TextView textViewResult;
    private Button btnClose;
    private Button btnLimpiar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextWeight = findViewById(R.id.editTextWeight);
        editTextHeight = findViewById(R.id.editTextHeight);
        textViewResult = findViewById(R.id.textViewResult);
        btnClose = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewResult.setText("");
                editTextHeight.setText(null);
                editTextWeight.setText(null);
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void calculateIMC(View view) {
        String weightStr = editTextWeight.getText().toString();
        String heightStr = editTextHeight.getText().toString();

        if (!weightStr.isEmpty() && !heightStr.isEmpty()) {
            float weight = Float.parseFloat(weightStr);
            float height = Float.parseFloat(heightStr);

            float imc = weight / (height * height);

            String result;
            if (imc < 18.5) {
                result = "Bajo peso";
            } else if (imc < 25) {
                result = "Peso normal";
            } else if (imc < 30) {
                result = "Sobrepeso";
            } else {
                result = "Obesidad";
            }

            textViewResult.setText("IMC: " + imc + "\n" + result);
        } else {
            textViewResult.setText("Ingrese el peso y la altura");
        }
    }
}